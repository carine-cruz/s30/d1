const express = require('express');
const app = express();
const dotenv = require('dotenv');
dotenv.config();
const mongoose = require('mongoose');
const PORT = 3006;

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Mongoose connection
    //mongoose.connection(<connection string>, {options});
    //process.env = ${variable}, MONGO_URL in .env
mongoose.connect(process.env.MONGO_URL,{useNewUrlParser: true, useUnifiedTopology: true});

//DB connection notification (test connection)
    //user connection property of mongoose
const db = mongoose.connection
db.on('error', console.error.bind(console, 'Connection error:')); //return issue upon error
db.once("open", () => console.log(`Connected to database`)); //successful connection

//SCHEMA

//Create a schema for tasks
    //schema determines the structure of the the documents to be written in the database
    //schema acts as a blueprint to our data
const taskSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, `Name is required`]
            },
        status: {
            type: String,
            default: "pending"
        }
    }
)

//create a model out of the schema
 //SYNTAX: mongoose.model(<name of the model>,<schema where model came from>)

 //model is a programming interface that enables us to query and manipulate database using its methods
 const Task = mongoose.model(`Task`, taskSchema);

 //ROUTES
 //Business Logic
 /*
 1. Add a functionality to check if there are duplicate tasks
    - If the task already exists in the database, we return an error
    -If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because schema defaults it to "pending" upon creation of an object
 */

app.post("/tasks", (req, res) => {
    //console.log(req.body)

    //search for document
    //method returns promise, thus use .then() to handle 
   Task.findOne({name: req.body.name}).then((result, err) => {
       console.log(result) //document

       //if task exists in the database, we return a message 'Duplicate task found'
       if (result != null && result.name == req.body.name){
           return res.send(`Duplicate Task found.`)
       } else {

           let newTask = new Task({
               name: req.body.name
           });

           //save document in database
           //use save() method to insert the new document in the database
           newTask.save().then((savedTasks, err) => {
               //console.log(result);
               if(savedTasks){
                   return res.send(result);
               } else {
                   return res.status(500).send(err);
               }
           })
       }
   })
})

/*
 Business Logic for getting all the tasks

 1. Retrieve all the documents (array)
 2. If an error is encountered, print the error
 3. If no errors are found, send a success status back to the Postman and return an array of the documents
*/

//no req.body
// app.get('/tasks', (req, res) => {
//     Task.find().then((docs, err)=>{
//         if (docs){
//             return res.status(200).send(docs);
//         } else{
//             return res.status(500).json(err);
//                 //
//         }
//     })
// })

//async - simultaeneous
app.get('/tasks', async(req, res) => {
    Task.find().then((docs, err)=>{
        if (docs){
            return res.status(200).send(docs);
        } else{
            return res.status(500).json(err);
                //
        }
    })
})

app.listen(PORT, () => console.log(`Server connected at port ${PORT}`));

